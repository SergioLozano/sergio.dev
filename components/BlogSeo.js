import React from 'react';
import { NextSeo, ArticleJsonLd } from 'next-seo';

const BlogSeo = ({ title, summary, publishedAt, url, image, author }) => {
  const date = new Date(publishedAt).toISOString();
  const featuredImage = {
    url: `https://sergio.dev${image}`,
    alt: title
  };

  return (
    <>
      <NextSeo
        title={`${title} – Sergio Lozano`}
        description={summary}
        canonical={url}
        openGraph={{
          type: 'article',
          article: {
            publishedTime: date
          },
          url,
          title,
          description: summary,
          images: [featuredImage]
        }}
      />
      <ArticleJsonLd
        authorName={author}
        dateModified={date}
        datePublished={date}
        description={summary}
        images={[featuredImage]}
        publisherLogo="/static/favicons/android-chrome-192x192.png"
        publisherName="Sergio Lozano"
        title={title}
        url={url}
      />
    </>
  );
};

export default BlogSeo;
