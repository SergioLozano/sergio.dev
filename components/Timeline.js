import React, { useState } from 'react';
import {
  useColorMode,
  Heading,
  Text,
  Flex,
  Button,
  List,
  ListItem,
  Icon,
  Stack,
  Divider
} from '@chakra-ui/core';

const YearDivider = () => {
  const { colorMode } = useColorMode();
  const borderColor = {
    light: 'gray.200',
    dark: 'gray.600'
  };

  return <Divider borderColor={borderColor[colorMode]} my={8} w="100%" />;
};

const TimelineStep = ({ title, children }) => {
  const { colorMode } = useColorMode();
  const color = {
    light: 'gray.700',
    dark: 'gray.400'
  };

  return (
    <ListItem>
      <Stack ml={2} mb={4}>
        <Flex align="center">
          <Icon name="check-circle" mr={2} color="whatsapp.500" />
          <Text fontWeight="medium">{title}</Text>
        </Flex>
        <Text color={color[colorMode]} ml={6}>
          {children}
        </Text>
      </Stack>
    </ListItem>
  );
};

const FullTimeline = () => (
  <>
    <YearDivider />
    <Heading as="h3" size="lg" fontWeight="bold" mb={4} letterSpacing="tighter">
      2017
    </Heading>
    <List>
      <TimelineStep title="Graduado en Multimedia 📜">
        Termino mi carrera universitaria y con ello pongo fin a una etapa.
      </TimelineStep>
      <TimelineStep title="Programador Junior ⚛️">
        Comienzo como programador junior en una pequeña startup. La experiencia
        no es muy buena, sin embargo, todo tiene un punto positivo y es que
        descubro react, mi framework favorito hasta la fecha.
      </TimelineStep>
      <TimelineStep title="La locura de emprender 🤪">
        Que otras personas comiencen a utilizar lo que programo en mi propia
        casa. Esta frase se repite en mi cabeza todo el rato... comienza la fase
        de emprender. Entro dentro de programas de emprendimiento como Emprèn
        UPC.
      </TimelineStep>
    </List>
    <YearDivider />
    <Heading as="h3" size="lg" fontWeight="bold" mb={4} letterSpacing="tighter">
      2016
    </Heading>
    <List>
      <TimelineStep title="Más progrmación y menos edición 🤖">
        La balanza comienza a equilibrarse, comienzo a ver la ventajas de
        programar y como me puede ayudar a convertir ideas en proyectos reales.
        La asignatura de programación de videojuegos ayuda bastante a meterme en
        el "mundillo".
      </TimelineStep>
      <TimelineStep title="Editor de video 🚗🎥">
        Realizo mis prácticas universitarias como editor de video. Me encanta
        trabajar de ello e ir a rodajes.
      </TimelineStep>
    </List>
    <YearDivider />
    <Heading as="h3" size="lg" fontWeight="bold" mb={4} letterSpacing="tighter">
      2014
    </Heading>
    <List>
      <TimelineStep title="Nuevo año nueva mentalidad 🎥💡">
        Ambos semestres tienen muchas más asiganturas audiovisuales, las cuales
        me gustan mucho más. Entro en el curso con más ganas que nunca.
      </TimelineStep>
      <TimelineStep title="Viaje a alemania (Gamescom) 👾">
        ¿Un viaje a Alemania con un amigo de la universidad a la mayor feria de
        videojuegos de Europa? ¿Qué podría salir mal? La verdad, nada.
      </TimelineStep>
    </List>
    <YearDivider />
    <Heading as="h3" size="lg" fontWeight="bold" mb={4} letterSpacing="tighter">
      2013
    </Heading>
    <List>
      <TimelineStep title="Graduación en el instituto 🧑‍🎓">
        Momentos divertidos, momentos no tan divertidos, el instituto está para
        eso.
      </TimelineStep>
      <TimelineStep title="Comienzo el grado en la Universidad Politécnica De Cataluña 🏗🌅">
        Entro en el grado de Multimedia interesado por la edición de video.
        Moverme de mi ciudad natal a Barcelona es sin duda una de las mejores
        experiencias que he podido vivir, buenos momentos y experiencias.
      </TimelineStep>
      <TimelineStep title="Abandonen el barco">
        El primer año es duro, la programación y la usabilidad (UX) se me
        atragantan. No consigo entender bien muchas cosas y mi desinterés no
        ayuda. Termino las asignaturas como puedo.
      </TimelineStep>
    </List>
    <YearDivider />
    <Heading as="h3" size="lg" fontWeight="bold" mb={4} letterSpacing="tighter">
      2006
    </Heading>
    <List>
      <TimelineStep title="Mi primer ordenador">
        Todo un abanico de posibilidades, pero yo me quedo con el Age of Empires
        y el pinball de windows vista.
      </TimelineStep>
    </List>
    <YearDivider />
    <Heading as="h3" size="lg" fontWeight="bold" mb={4} letterSpacing="tighter">
      2001
    </Heading>
    <List>
      <TimelineStep title="Nintendo llega a casa 🕹️">
        GameBoy Advance... No sé ni como sigue funcionando...
      </TimelineStep>
    </List>
    <YearDivider />
    <Heading as="h3" size="lg" fontWeight="bold" mb={4} letterSpacing="tighter">
      1995
    </Heading>
    <List>
      <TimelineStep title="Aterrizaje 🐣🍼">
        Al fin y al cabo toda historia tiene un comienzo ¿no?
      </TimelineStep>
    </List>
  </>
);

const Timeline = () => {
  const [isShowingFullTimeline, showFullTimeline] = useState(false);

  return (
    <Flex
      flexDirection="column"
      justifyContent="flex-start"
      alignItems="flex-start"
      maxWidth="700px"
      mt={8}
    >
      <Heading letterSpacing="tight" mb={4} size="xl" fontWeight="bold">
        Línea Temporal
      </Heading>
      <Heading
        as="h3"
        size="lg"
        fontWeight="bold"
        mb={4}
        letterSpacing="tighter"
      >
        2020
      </Heading>
      <List>
        <TimelineStep title="Viaje a Grecia 🏛 🇬🇷">
          Uno de mis mejores viajes hasta la fecha. Atenas es una ciudad que
          desprende arte y cultura por todos sus rincones. La comida, los bares
          y los momentos que me ha dado Atenas han sido increíbles.
        </TimelineStep>
        <TimelineStep title="Nutrición y cáncer 🍲🦀">
          Nuevo año nuevo proyecto y Nutrilieve llama a mi puerta. Se me ofrece
          la oportunidad de crear una plataforma para unir a nutricionistas y
          pacientes de cáncer.
        </TimelineStep>
      </List>
      <YearDivider />
      <Heading
        as="h3"
        size="lg"
        fontWeight="bold"
        mb={4}
        letterSpacing="tighter"
      >
        2019
      </Heading>
      <List>
        <TimelineStep title="Melone en la apple store 🍎">
          Depués de muchos quebraderos de cabeza conseguimos entrar en la iOS
          store. ¡Melone ya está disponible para iphone!
        </TimelineStep>
        <TimelineStep title="De charla en charla 🎤">
          Mi socio de Melone y yo movemos tierra y cielo para que nuestra app
          llegue a más gente. Sin embargo, la carrera de las aplicaciones de
          escaneo de productos había comenzado y en España aparece un fuerte
          competidor que nos impide seguir adelante.
        </TimelineStep>
        <TimelineStep title="Melone App 🍈">
          Nace la idea de crear Melone una App capaz de escanear cualquier
          producto de supermercado y clasificarlo en saludable o poco saludable.
        </TimelineStep>
      </List>
      <YearDivider />
      <Heading
        as="h3"
        size="lg"
        fontWeight="bold"
        mb={4}
        letterSpacing="tighter"
      >
        2018
      </Heading>
      <List>
        <TimelineStep title="Master en Desarrollo de Aplicaciones Móviles 📱">
          Noto que me faltan conocimientos en desarrollo y me centro en
          ampliarlos. Aprendo mucho sobre kotlin, Java, Swift...Se abren nuevas
          puertas.
        </TimelineStep>
        <TimelineStep title="Vuelta a mis origenes 🏠❤️">
          Tras centrarme varios meses en mejorar mis habilidades como
          programador regreso a mi ciudad natal (Burgos). Aprovecho para
          reorganizar muchos aspectos de mi vida personal.
        </TimelineStep>
        <TimelineStep title="Viaje a Japón ✈️🌸">
          Pasarán años y seguiré hablando de este viaje. La gente se esta
          cansando de escucharme, asi que solo me queda volver para repetir
          experiencias y descubrir otras nuevas, me queda mucho que explorar de
          este maravilloso país.
        </TimelineStep>
      </List>

      {isShowingFullTimeline ? (
        <FullTimeline />
      ) : (
        <Button
          my={4}
          mx="auto"
          fontWeight="medium"
          rightIcon="chevron-down"
          variant="ghost"
          onClick={() => showFullTimeline(true)}
        >
          Ver más
        </Button>
      )}
    </Flex>
  );
};

export default Timeline;
