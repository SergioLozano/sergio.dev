import React from 'react';
import { parseISO, format } from 'date-fns';
import {
  useColorMode,
  Heading,
  Text,
  Flex,
  Stack,
  Avatar,
  Link,
  Box,
  Alert,
  AlertTitle,
  AlertDescription,
  AlertIcon,
  Icon
} from '@chakra-ui/core';

import Container from '../components/Container';
import Subscribe from '../components/Subscribe';
import ViewCounter from '../components/ViewCounter';
import BlogSeo from '../components/BlogSeo';

const editUrl = (slug) =>
  `https://gitlab.com/SergioLozano/sergio.dev/-/blob/main/pages/blog/${slug}.mdx`;
const discussUrl = (slug) =>
  `https://mobile.twitter.com/search?q=${encodeURIComponent(
    `https://sergio.dev/blog/${slug}`
  )}`;

export default (frontMatter) => {
  const slug = frontMatter.__resourcePath
    .replace('blog/', '')
    .replace('.mdx', '');

  return ({ children }) => {
    const { colorMode } = useColorMode();
    const textColor = {
      light: 'gray.700',
      dark: 'gray.400'
    };

    return (
      <Container>
        <BlogSeo url={`https://sergio.dev/blog/${slug}`} {...frontMatter} />
        <Stack
          as="article"
          spacing={8}
          justifyContent="center"
          alignItems="flex-start"
          m="0 auto 4rem auto"
          maxWidth="700px"
          w="100%"
        >
          <Flex
            flexDirection="column"
            justifyContent="flex-start"
            alignItems="flex-start"
            maxWidth="700px"
            w="100%"
          >
            <Heading letterSpacing="tight" mb={2} as="h1" size="2xl">
              {frontMatter.title}
            </Heading>
            <Flex
              justify="space-between"
              align={['initial', 'center']}
              direction={['column', 'row']}
              mt={2}
              w="100%"
              mb={4}
            >
              <Flex align="center">
                <Avatar
                  size="sm"
                  name="Sergio Lozano"
                  src="https://bit.ly/36qoquo"
                  mr={2}
                />
                <Box>
                  <Text fontSize="sm" color={textColor[colorMode]}>
                    {'Autor: '}
                    <Link href={frontMatter.originalLink} isExternal>
                      {frontMatter.author}
                    </Link>
                  </Text>
                  <Text fontSize="sm" color={textColor[colorMode]}>
                    {'Traducción: Sergio Lozano'}
                  </Text>
                </Box>
              </Flex>
              <Box>
                <Text
                  fontSize="sm"
                  color="gray.500"
                  minWidth="100px"
                  mt={[2, 0]}
                >
                  {format(parseISO(frontMatter.publishedAt), 'MMMM dd, yyyy')}
                  {` • `}
                  <ViewCounter id={slug} />
                </Text>
                <Text
                  fontSize="sm"
                  color="gray.500"
                  minWidth="100px"
                  mt={[2, 0]}
                >
                  {`Lectura ${Math.ceil(frontMatter.readingTime.minutes)} min`}
                </Text>
              </Box>
            </Flex>
          </Flex>
          <Box as="table" textAlign="left" mt="32px" width="full">
            <Alert status="info" variant="top-accent">
              <AlertIcon />
              <AlertTitle mr={2}>Versión traducida</AlertTitle>
              <AlertDescription>
                {` puedes leer artículo orginal de ${frontMatter.author} `}
                <Link href={frontMatter.originalLink} isExternal>
                  aquí <Icon name="external-link" mx="2px" />
                </Link>
              </AlertDescription>
            </Alert>
          </Box>
          {children}
          <Subscribe />
          <Box>
            <Link href={discussUrl(slug)} isExternal>
              {'Comenta en Twitter'}
            </Link>
            {` • `}
            <Link href={editUrl(slug)} isExternal>
              {'Edita en GitLab'}
            </Link>
          </Box>
        </Stack>
      </Container>
    );
  };
};
