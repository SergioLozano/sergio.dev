const title = 'Sergio Lozano – Desarrollador, creador, y traductor.';
const description =
  'Desarrollador Full-stack, trabajando con JavaScript, Node y bases de datos nosql.';

const SEO = {
  title,
  description,
  canonical: 'https://sergio.dev',
  openGraph: {
    type: 'website',
    locale: 'es_ES',
    url: 'https://sergio.dev',
    title,
    description,
    images: [
      {
        url: 'https://sergio.dev/static/images/og.jpg',
        alt: title,
        width: 1280,
        height: 720
      }
    ]
  },
  twitter: {
    handle: '@seergioldr',
    site: '@seergioldr',
    cardType: 'summary_large_image'
  }
};

export default SEO;
