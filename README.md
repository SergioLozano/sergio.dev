# sergio.dev

Tras varias semanas pensando cómo aportar mi granito de arena a la comunidad de desarrollo de software, me topé con el blog de [Lee Robinson](https://leerob.io). Un blog al que aún a día de hoy, acudo a menudo para obtener información, o resolver dudas. Lo mejor es que Lee ofrece su blog en formato open source, por lo que me decidi a utilizar su código y adaptarlo a mis necesidades.

Mediante el uso de React/Next.js/MDX puedo escribir y traducir artículos y colgarlos en mi página. Ahora puedo guardar artículos interesantes de una forma mucho más ordenada y compartirlos con el resto del mundo.

En mi blog me dedico a traducir los artículos que más me gustan al español. Aunque se que la comunidad del software se mueve entorno al inglés, internacionalizar la información es muy importante para mantener la diversidad de idiomas, que a día de hoy podemos disfrutar.

## Clonar el blog de LeeRob

```bash
$ git clone https://github.com/leerob/leerob.io.git
$ cd sergio.dev
$ yarn
$ yarn dev
```

Create a `.env.local` file similar to `.env`.

```
FIREBASE_PRIVATE_KEY=
FIREBASE_CLIENT_EMAIL=
UNSPLASH_ACCESS_KEY=
GOOGLE_ENCRYPTION_KEY=
GOOGLE_ENCRYPTION_IV=
BUTTONDOWN_API_KEY=
```

## Resumen

- `pages/api/*` - [API routes](https://nextjs.org/docs/api-routes/introduction) powering [`/dashboard`](https://leerob.io/dashboard), newsletter subscription, and post views.
- `pages/blog/*` - Static pre-rendered blog pages using [MDX](https://github.com/mdx-js/mdx).
- `pages/dashboard` - [Personal dashboard](https://leerob.io/dashboard) containing metrics like sales, views, and subscribers.
- `pages/*` - All other static pages.

## Tecnologías utilizadas

- [Next.js](https://nextjs.org/)
- [Vercel](https://vercel.com)
- [MDX](https://github.com/mdx-js/mdx)
- [Chakra UI](https://chakra-ui.com/)
