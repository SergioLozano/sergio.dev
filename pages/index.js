import React from 'react';
import { useColorMode, Heading, Text, Flex, Stack, Link } from '@chakra-ui/core';

import Timeline from '../components/Timeline';
import Container from '../components/Container';
import BlogPost from '../components/BlogPost';
import Subscribe from '../components/Subscribe';
import ProjectCard from '../components/ProjectCard';
import Footer from '../components/Footer';

import { frontMatter as styleGuides } from './blog/guias-de-estilo-diseño-de-sistemas.mdx';
import { frontMatter as monorepo } from './blog/monorepo-lerna-yarn-workspaces.mdx';
import { frontMatter as styledComponents } from './blog/biblioteca-de-componentes-ui-con-styled-components.mdx';

const Index = () => {
  const { colorMode } = useColorMode();
  const secondaryTextColor = {
    light: 'gray.700',
    dark: 'gray.400'
  };

  const linkColor = {
    light: 'hsl(187, 100%, 44%)',
    dark: 'hsl(164, 95%, 69%)'
  };

  return (
    // <Container>
    //   <Stack
    //     as="main"
    //     spacing={8}
    //     justifyContent="center"
    //     alignItems="flex-start"
    //     m="0 auto 4rem auto"
    //     maxWidth="700px"
    //   >
    //     <Flex
    //       flexDirection="column"
    //       justifyContent="flex-start"
    //       alignItems="flex-start"
    //       maxWidth="700px"
    //     >
    //       <Heading letterSpacing="tight" mb={2} as="h1" size="2xl">
    //         ¡Hola! Soy Sergio Lozano
    //       </Heading>
    //       <Text color={secondaryTextColor[colorMode]}>
    //         Desarrollador, emprendedor y desde que tengo este blog, traductor.
    //         Por las mañanas suelo leer diferentes artículos sobre desarrollo de
    //         software, aquí encontrás los más interesantes, traducidos al español
    //         – porque... ¡no todo va a estar en inglés!
    //       </Text>
    //     </Flex>
    //     <Flex
    //       flexDirection="column"
    //       justifyContent="flex-start"
    //       alignItems="flex-start"
    //       maxWidth="700px"
    //       mt={8}
    //     >
    //       <Heading letterSpacing="tight" mb={4} size="xl" fontWeight={700}>
    //         Artículos recientes
    //       </Heading>
    //       <BlogPost {...styleGuides} />
    //       <BlogPost {...styledComponents} />
    //       <BlogPost {...monorepo} />
    //     </Flex>
    //     <Flex
    //       flexDirection="column"
    //       justifyContent="flex-start"
    //       alignItems="flex-start"
    //       maxWidth="700px"
    //     >
    //       <Heading letterSpacing="tight" mb={4} size="xl" fontWeight={700}>
    //         Mis proyectos
    //       </Heading>
    //       <ProjectCard
    //         title="Nutrilieve"
    //         description="En Nutrilieve hemos creado un sistema que permite procesar datos nutricionales y convertirlos en recetas concretas aptas para pacientes oncológicos."
    //         href="https://nutrilieve.com/"
    //         icon="nutrilieve"
    //       />
    //       <ProjectCard
    //         title="Melone"
    //         description="Melone te ayuda a cumplir tus objetivos nutricionales y a mejorar la calidad de los alimentos que consumes. Escanea cualquier producto en solo segundos."
    //         href="https://melone.io/"
    //         icon="melone"
    //       />
    //     </Flex>
    //     <Timeline />
    //     <Subscribe />
    //   </Stack>
    // </Container>
    <Stack
      as="main"
      spacing={8}
      justifyContent="center"
      alignItems="flex-start"
      m="4rem auto 4rem auto"
      maxWidth="700px"
    >
      <Flex
        flexDirection="column"
        justifyContent="flex-start"
        alignItems="flex-start"
        maxWidth="700px"
      >
        <Heading letterSpacing="tight" mb={2} as="h1" size="2xl">
          ¡Hola! Soy Sergio
          </Heading>
        <Heading as="h3" size="lg" mb={2}>
          Voy a reconstruir mi página web
          </Heading>
        <Text mb={6} color={secondaryTextColor[colorMode]}>
          Los últimos meses he estado trabajando en diferentes proyectos 👨‍💻🔨.
          Ahora que al fin se acercan tiempos de calma, espero poder dedicar más tiempo a este pequeño ricón de internet.
          </Text>

          <Text color={secondaryTextColor[colorMode]}>
          Sigo abierto a proyectos y trabajos freelance.
          </Text>
        <Text color={secondaryTextColor[colorMode]}>
          Contacta conmigo en: <Link
            href="mailto:me@sergio.dev"
            title="Email"
            color={linkColor[colorMode]}
            isExternal
          >
            me@sergio.dev
              </Link>
        </Text>
      </Flex>
      <Flex
        flexDirection="column"
        justifyContent="flex-start"
        alignItems="flex-start"
        maxWidth="700px"
      >
        <Heading letterSpacing="tight" mb={4} size="xl" fontWeight={700}>
          Mis proyectos
          </Heading>
        <ProjectCard
          title="Nutrilieve"
          description="En Nutrilieve hemos creado un sistema que permite procesar datos nutricionales y convertirlos en recetas concretas aptas para pacientes oncológicos."
          href="https://nutrilieve.com/"
          icon="nutrilieve"
        />
        <ProjectCard
          title="Melone"
          description="Melone te ayuda a cumplir tus objetivos nutricionales y a mejorar la calidad de los alimentos que consumes. Escanea cualquier producto en solo segundos."
          href="https://melone.io/"
          icon="melone"
        />
      </Flex>

      <Flex
        mt={10}
        justify="center"
        align="center"
        minWidth="700px">
        <Footer />
      </Flex>

    </Stack>

  );
};

export default Index;
