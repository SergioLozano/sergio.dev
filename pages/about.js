import React from 'react';
import { NextSeo } from 'next-seo';
import {
  useColorMode,
  Heading,
  Text,
  Flex,
  Stack,
  Link
} from '@chakra-ui/core';

import Container from '../components/Container';

const url = 'https://sergio.dev/about';
const title = 'Sobre mí – Sergio Lozano';

const About = () => {
  const { colorMode } = useColorMode();
  const secondaryTextColor = {
    light: 'gray.700',
    dark: 'gray.400'
  };
  const linkColor = {
    light: 'hsl(187, 100%, 44%)',
    dark: 'hsl(164, 95%, 69%)'
  };

  return (
    <>
      <NextSeo
        title={title}
        canonical={url}
        openGraph={{
          url,
          title
        }}
      />
      <Container>
        <Stack
          as="main"
          spacing={8}
          justifyContent="center"
          alignItems="flex-start"
          m="0 auto 4rem auto"
          maxWidth="700px"
        >
          <Flex
            flexDirection="column"
            justifyContent="flex-start"
            alignItems="flex-start"
            maxWidth="700px"
          >
            <Heading letterSpacing="tight" mb={2} as="h1" size="2xl">
              Sobre mí
            </Heading>
            <Text color={secondaryTextColor[colorMode]} mb={4}>
              Hola, Soy Sergio. Actualmente vivo en Burgos, España y soy
              desarrollador de aplicaciones web y móviles. A día de hoy mi
              prioridad es&nbsp;
              <Link
                href="https://nutrilieve.com/about"
                title="Nutrilieve"
                isExternal
                color={linkColor[colorMode]}
              >
                Nutrilieve
              </Link>
              &nbsp;una startup de nutrición oncológica que he desarrollado
              junto a mis socios durante los últimos meses. El objetivo de esta
              startup es ayudar a pacientes de cáncer a paliar los síntomas del
              tratamiento mediante planes nutricionales y recetas específicas.
              Como desarrollador me dedico a mantener el código de la plataforma
              al día para que nuestros usuarios puedan seguir utilizando sus
              diferentes funciones.
            </Text>
            <Text color={secondaryTextColor[colorMode]} mb={4}>
              A finales de 2019 lancé&nbsp;
              <Link
                href="https://melone.io"
                title="Melone App"
                isExternal
                color={linkColor[colorMode]}
              >
                Melone
              </Link>
              &nbsp;una aplicación para iOS y Android capaz de escanear el
              código de barras de los productos alimenticios y que los clasifica
              según su aporte nutricional.
            </Text>
            <Text color={secondaryTextColor[colorMode]} mb={4}>
              Gracias a mis conocimientos adquiridos como video-editor y mi
              preocupación por el diseño y la usabilidad soy capaz de trabajar
              de forma proactiva en diferentes áreas como diseño de interfaces o
              márketing digital.
            </Text>
            <Text color={secondaryTextColor[colorMode]} mb={4}>
              Si estás interesado en mi carrera profesional, echa un vistazo a
              mi&nbsp;
              <Link
                href="/sergio-lozano-currículum-2020.pdf"
                title="Curriculum"
                color={linkColor[colorMode]}
              >
                curriculum.
              </Link>
              &nbsp;O contacta conmigo a través de mi email{' '}
              <Link
                href="mailto:me@sergio.dev"
                title="Email"
                color={linkColor[colorMode]}
                isExternal
              >
                me@sergio.dev
              </Link>
            </Text>
            <Text color={secondaryTextColor[colorMode]} mb={4}>
              Me encanta jugar con nuevas tecnologías, explotar su potencial y
              ver como sacar el máximo provecho de ellas. Mi tiempo libre lo
              dedico a ver series, películas, jugar y estar con las personas que
              verdaderamente me importan, mi familia y mis amigos.
            </Text>
          </Flex>
        </Stack>
      </Container>
    </>
  );
};

export default About;
