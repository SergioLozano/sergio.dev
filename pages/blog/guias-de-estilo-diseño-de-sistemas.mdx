---
title: 'Todo lo que se acerca de guías de estilo, Diseño de sistemas, y Librerías de componentes'
publishedAt: '2020-05-20'
summary: 'Estudio en profundidad de todo lo que he aprendido en el último año creando guías de estilo, diseñando sistemas, librerías de componentes y sus mejores prácticas.'
image: '/static/images/guias-de-estilo-diseño-de-sistemas/banner.jpg'
author: 'Lee Robinson'
originalLink: 'https://leerob.io/blog/style-guides-component-libraries-design-systems'
---

[![Ant Design Component Library](/static/images/guias-de-estilo-diseño-de-sistemas/component-library.png)](https://ant.design/)

Durante este último año, he invertido la mayor parte de mi tiempo en desarrollo front-end y diseño. Cuando comencé en mi nuevo puesto en Hy-Vee, identifiqué la necesidad de una librería de componentes, así que la creé. Desde entonces, he aprendido muchísimo sobre guías de estilo, diseño de sistemas, librerías de componentes y sus mejores prácticas. Este post profundiza en detalle todo lo que he ido aprendiendo en este último año.

## Tabla de Contenidos

1.  [¿Por qué Debería Importarte?](#por-que-deberia-importarte)
2.  [¿Qué Es una Guía de Estilo?](#que-es-una-guia-de-estilo)
3.  [¿Qué Es una Librería de Componentes?](#que-es-una-libreria-de-componentes)
4.  [¿Qué Es un Diseño de Sistema?](#que-es-un-diseno-de-sistema)
5.  [¿Por Dónde Deberías Empezar?](#por-donde-deberias-empezar)
6.  [Desarrollando un Diseño de Sistema](#desarrollando-un-diseno-de-sistema)
7.  [Conclusión](#conclusion)
8.  [Recursos](#recursos)

## ¿Por Qué Debería Importarte?

Todo sitio web empieza de forma simple. A lo mejor hay una página con cierta cantidad de piezas. Tiene unas intenciones modestas.

[![Website Mockup](/static/images/guias-de-estilo-diseño-de-sistemas/website.png)](https://refactoringui.com/)

Pero, poco a poco, comienza a escalar y hacerse más grande.

Se añaden más páginas y se desarrollan nuevas funcionalidades. Se puede dar el caso de que múltiples equipos participen en el desarrollo de una sección específica del sitio web. Podría ser que, al mismo tiempo, desarrolles para dispositivos móviles.

De pronto comienzas a notar que los botones de una determinada parte de tu página web son algo diferentes que en el resto. Un equipo decide desarrollar una función que otro equipo (sin saberlo) ya ha completado. Los fallos de comunicación existen y la consistencia se pierde.

¿Se trata de un problema que se puede prevenir? Por supuesto. Entonces, ¿Por qué ocurre una y otra vez? **Si no piensas en el diseño y en el proceso de desarrollo de forma previa, cuanto más grande se haga tu proyecto, más problemas vas a encontrar.**

Para prevenir este tipo de problemas, o solucionar la falta de consistencia en tus productos, necesitas tres elementos:

- **Guía de Estilo**
- **Librería de Componentes**
- **Diseño de sistema**

No te preocupes si aún no tienes ni idea de lo que te estoy hablando. Al final de este artículo, habrás consumado como mínimo un año de prueba y error, investigación y desarrollo.

## ¿Qué Es una Guía de Estilo?

[![Style Guide](/static/images/guias-de-estilo-diseño-de-sistemas/style-guide.png)](https://dribbble.com/shots/6161977-BlueVine-Product-Style-Guide)

Una **guía de estilo** es un conjunto de reglas sobre cómo debe mostrarse tu marca. Esto es tanto visual (diseño e imágenes) como escrito (voz y tono).

El propósito de una guía de estilo es **permitir a múltiples colaboradores crear un contenido claro que represente de forma coherente la marca**. Casi todas las grandes marcas tienen una guía de estilo, aunque no todas son públicas.

## ¿Qué Es una Librería de Componentes?

[![Component Library](/static/images/guias-de-estilo-diseño-de-sistemas/components.png)](https://eva.design/)

Una **librería de componentes** es la viva imagen de tu guía de estilo. Se trata de un conjunto compartido de componentes que forman la interfaz de usuario. Los desarrolladores pueden consumir este recurso y construir aplicaciones que reflejen tu marca.
Algunos beneficios notables son:

- Tener una librería de componentes supone tener menos código propio.
- Puesto que todo se encuentra centralizado, puedes asegurarte de que todos los componentes cumplen con los requisitos de accesibilidad.
- Los componentes se vuelven más robustos si cuentan con múltiples colaboradores que envían soluciones para errores, mejoras y todo ello desde un mismo lugar.
- A menor duplicación en el código mayor capacidad para enviar nuevos productos y reescribir código heredado.

## ¿Qué Es un Diseño de Sistema?

Un **diseño de sistema** es un completo set de estándares, documentación y normas a lo largo de los componentes para lograr esos estándares. Se trata de **la unión entre tu guía de estilo y tu librería de componentes**. Uno de los diseños de sistemas más populares es Google [Material Design](https://material.io/design/).

[![Design System](/static/images/guias-de-estilo-diseño-de-sistemas/design-system.png)](https://www.uxpin.com/studio/blog/design-systems-vs-pattern-libraries-vs-style-guides-whats-difference/)

Un diseño de sistema debe contener:

- **Contenido** -- El lenguaje para diseñar una experiencia de producto meditada y cuidada.
- **Diseño** -- La parte visual del diseño de sistema.
- **Componentes** -- Los ladrillos de la casa que te permiten desarrollar nuevos productos y funcionalidades.
- **Patrones** -- Piezas individuales que te permiten crear una experiencia significativa del producto para tus usuarios.

## ¿Por Dónde Deberías Empezar?

Dependiendo de el estado de tu empresa/producto, deberías de considerar diferentes puntos de partida. A continuación, te muestro un esquema antes de entrar en detalle.

- Crear un inventario para patrones de diseño existentes.
- Establecer las normas del diseño y comenzar una guía de estilo.
- Definir los tokens del diseño.
- Crear o encontrar un set de iconos.
- Escoger un soporte para lenguajes de programación/frameworks utilizados.
- Evaluar Monorepo vs. single package.
- Evaluar CSS/Sass vs. CSS-in-JS.
- Crear una librería de componentes.
- Escoger una plataforma para la documentación.
- Escribir la documentación del diseño de sistema.

## Desarrollando un Diseño de Sistema

### Crear un inventario para patrones de diseño existentes.

A menos que estés empezando de cero, necesitarás identificar todos los [patrones de diseño](https://pttrns.com/applications/86) que se encuentren en uso dentro de tu interfaz y documentar todas las inconsistencias.
El objetivo debe ser tener la misma experiencia de usuario, independientemente de la parte del producto en la que se encuentre.
Empieza a documentar y revisar todo esto con ayuda de tu equipo e identifica los patrones de interacción que más os gustan.
De esta forma podrás comenzar a dibujar un esquema visual con las piezas que necesitará tu guía de estilo.

### Establecer las normas del diseño y comenzar una guía de estilo.

Es hora de traducir nuestros descubrimientos en los inicios de una guía de estilo. Para ello puedes utilizar herramientas como:

- [Sketch](https://www.sketch.com/)
- [Figma](https://www.figma.com/)
- [Framer](https://www.framer.com/)
- [Abobe XD](https://www.adobe.com/products/xd.html)

Mi recomendación personal sería **Sketch**, pero es decisión de tu equipo y empresa. Asegurate de entender [las mejores prácticas sobre los símbolos de Sketch](https://novemberfive.co/blog/symbols-guidelines-in-sketch/) y cuando utilizar [símbolos anidados](https://www.sketch.com/docs/symbols/nested-symbols/).

### Definir los tokens del diseño.

[![Tokens de Diseño](/static/images/guias-de-estilo-diseño-de-sistemas/design-tokens.png)](https://www.antforfigma.com/)

> Los tokens de diseño (Design Tokens) son los átomos de diseño visual del sistema de diseño --- específicamente, son entidades nombradas que almacenan atributos del diseño visual. Los usamos en lugar de los valores codificados (como por ejemplo los valores hexadecimales par el color o los valores de los píxeles para el espaciado). Permiten mantener un sistema visual escalable y consistente para el desarrollo de la interfaz de usuario.

Estos Tokens incluyen elementos como:

- [Colores](https://colors.eva.design/) (text, backgrounds, borders)
- [Tipografías](https://fonts.google.com/)
- [Tamaños de tipografías](http://spencermortensen.com/articles/typographic-scale/)
- [Valores de espaciado](https://builttoadapt.io/intro-to-the-8-point-grid-system-d2573cde8632)
- [Sombreados](https://material.io/design/environment/light-shadows.html)

Por ejemplo, echa un vistazo al [Sistema de diseño de Vercel](https://vercel.com/design).

```css
--geist-foreground: #000;
--geist-background: #fff;
--accents-1: #fafafa;
--accents-2: #eaeaea;
--accents-3: #999999;
--accents-4: #888888;
--accents-5: #666666;
--accents-6: #444444;
--accents-7: #333333;
--accents-8: #111111;
--geist-success: #0070f3;
--geist-error: #ee0000;
--geist-warning: #f5a623;
--dropdown-box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.02);
--dropdown-triangle-stroke: #fff;
--scroller-start: var(--geist-background);
--scroller-end: rgba(255, 255, 255, 0);
--shadow-small: 0 5px 10px rgba(0, 0, 0, 0.12);
--shadow-medium: 0 8px 30px rgba(0, 0, 0, 0.12);
--shadow-large: 0 30px 60px rgba(0, 0, 0, 0.12);
--portal-opacity: 0.25;
```

Hoy por hoy, podemos decir que existe un lenguaje que comparten diseñadores y programadores.

### Crear o encontrar un set de iconos.

[![Set de iconos](/static/images/guias-de-estilo-diseño-de-sistemas/icons.png)](https://dribbble.com/shots/6632346-Zoom-Redesign-Icon-Library)

Si ya dispones de iconos, necesitarás determinar con cuales quieres quedarte. En muchos casos carece de sentido crear tus propios iconos, esto depende en gran medida, del tamaño y las prioridades de tu compañía. Cuando no necesitas crear tus propios iconos lo mejor es utilizar una librería de iconos de código abierto. Aquí te dejo algunos ejemplos:

- [Font Awesome](https://fontawesome.com)
- [Icons8](https://icons8.com/)
- [Material UI Icons](https://material-ui.com/components/icons/)
- [Eva Icons](https://akveo.github.io/eva-icons/)
- [Feather Icons](https://feathericons.com)
- [Styled Icons](https://styled-icons.js.org/)
- [Ikonate](https://ikonate.com/)

Independientemente de si creas tu set de iconos o utilizas uno ya hecho, tienes que normalizar y estandarizar patrones. También es de utilidad conocer quienes son los usuarios finales a los que va destinado tu set de iconos - diseñadores, desarrolladores o quizás un equipo de marketing.

Usando el formato de imagen SVG, puedes hacer que tu librería de iconos sea un recurso único. El proceso de uso puede ser similar al siguiente:

1.  Los diseñadores crean/proveen de un icono SVG a modo de materia prima.
2.  Optimiza y comprime el archivo SVG utilizando [SVGO](https://github.com/svg/svgo/).
3.  Automáticamente crea componentes de React utilizando [SVGR](https://github.com/smooth-code/svgr) para los desarrolladores.
4.  Exporta en formato PNG y JPEG a diferentes resoluciones para el equipo de marketing.
5.  Convierte los archivos SVG en un [Paquete de fuentes tipográficas](https://www.npmjs.com/package/icon-font-generator) para que pueda ser utilizado por aplicaciones móviles.

### Escoger un soporte para lenguajes de programación/frameworks utilizados.

¿Qué lenguajes o frameworks estas utilizando actualmente? ¿Cuales debería emplear la librería de componentes?
Es importante pensar en esto de antemano para que puedas estructurar adecuadamente tu librería.

Si tienes una aplicación que utiliza etiquetas en vez de [ES Modules](https://flaviocopes.com/es-modules/), tendrás que configurar toda tu librería de componentes para que pueda ser exportada en un solo archivo de distribución. Por ejemplo, una aplicación que utiliza JavaScript de forma plana (Vanilla) va a necesitar algo similar a esto en su archivo HTML.

```html
<script src="/js/component-library.min.js"></script>
```

Para llegar a este mismo punto, también puedes utilizar [Webpack](https://webpack.js.org/) o [Rollup](https://rollupjs.org/).
Personalmente recomiendo utilizar Webpack, ya que es un estándar en la industria.

### Evaluar Monorepo vs. single package.

[Monorepo](https://www.toptal.com/front-end/guide-to-monorepos) te permite construir y publicar múltiples librerías desde un mismo repositorio. Y aunque resuelve algunos problemas existentes, también crea otros. Es importante conocer los pros y contras.

[![Monorepo](/static/images/guias-de-estilo-diseño-de-sistemas/monorepo.png)](https://www.toptal.com/front-end/guide-to-monorepos)

**Pros**

- ✅ Empaquetado de menor tamaño reduciendo el ámbito de aplicación de los componentes a un solo paquete.
- ✅ Desarrollo compartido, linter, testeo y proceso de creación de múltiples paquetes frente a repositorios separados con código duplicado.
- ✅ Un control más granular de [semver](https://semver.org/) en cada paquete.

**Contras**

- ⛔️ Se necesitan herramientas e infraestructura adicional.
- ⛔️ Los consumidores del código tienen que importar múltiples paquetes en vez de una sola librería de componentes.
- ⛔️ Tecnología de vanguardia. La industria está adaptada, pero puedes encontrarte problemas que muy pocos han experimentado y no tener un camino claro para solucionarlo.

Algunas preguntas que te ayudarán a identificar la solución adecuada para tu empresa:

- ¿Haces uso de múltiples repositorios, todos ellos publicados en NPM?
- ¿Tu desarrollo, testeo o linter(lint) utilizan una estructura compleja? ¿Se duplica en muchos sitios?
- ¿Cuántos repositorios tienes (o planeas tener en el próximo año)?
- ¿Cómo de grande es tu equipo? ¿Cuántas personas harán uso de tu librería de componentes?
- ¿Será de código abierto?¿Puedes apoyarte en otros dentro de la industria y resolver diferentes problemas?
- ¿Ves la necesidad de tener múltiples paquetes en el futuro? (ej. iconos, [codemods](https://github.com/facebook/jscodeshift), etc)

Para la mayoría de las empresas pequeñas, yo diría que el uso de Monorepo es innecesario. Todo tiene su momento y su lugar. [Lo utilizamos en Hy-Vee](/blog/monorepo-lerna-yarn-workspaces) (~150 desarrolladores) para distribuir 10 paquetes diferentes al mismo tiempo, abarcando aplicaciones internas y externas.

**Nota:** ¿Quieres utilizar Monorepo? Echa un vistazo a [Creando un Monorepo con Lerna & Yarn Workspaces](/blog/monorepo-lerna-yarn-workspaces).

### Evaluar CSS/Sass vs. CSS-in-JS.

He llegado a preferir CSS-in-JS, especialmente [styled-components](https://www.styled-components.com/). Existe una gran variedad de opciones para CSS-in-JS [listado](https://github.com/MicheleBertoli/css-in-js). Las principales razones que nos llevaron a utilizar styled-components para nuestra librería de componentes fueron:

- El parecido de escribir CSS tradicional en comparación a escribir objetos de JavaScript
- Soporte para React y React Native
- Pre-marcado automático en los estilos
- Estilos directos que eliminan conflictos de CSS globales

Si tu librería de componentes necesita soporte para exportar HTML/CSS sin procesar, recomiendo seguir utilizando Sass. Un gran ejemplo de esto es [Carbon Design System](https://www.carbondesignsystem.com/components/button/code) de IBM.
![Diseño de Sistemas Botón](/static/images/guias-de-estilo-diseño-de-sistemas/button.png)

```html
<button class="bx--btn bx--btn--primary" type="button">
  Button
</button>
```

Independientemente de la tecnología que elijas, recomendaría usar [variables CSS](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties) para mantener tus [tokens de diseño](https://www.lightningdesignsystem.com/design-tokens/).

### Crear una librería de componentes.

Con los requisitos técnicos bien definidos y la base de una guía de diseño, ya deberías de ser capaz de empezar a construir una librería de componentes. Mi recomendación es que utilices **React alongside [Storybook](https://storybook.js.org/)**.

[![Desarrollo de componentes](/static/images/guias-de-estilo-diseño-de-sistemas/component-driven-development.jpg)](https://storybook.js.org/)

**Crear una librería de componentes es mucho más que solo trasladar la guía de estilo a código.**

Vas a tener que pensar cómo los consumidores van a utilizar tus componentes. ¿Qué clase de API esperan encontrar? ¿Qué proporciona mayor claridad y es capaz de auto documentarse?
Por ejemplo, analiza los botones. Tienes múltiples variaciones de botones - primario, secundario, etc.

[![Botones](/static/images/guias-de-estilo-diseño-de-sistemas/buttons.png)](https://eva.design/)

¿Deberías tener componentes separados para cada uno?

```jsx
<PrimaryButton>Hello World!</PrimaryButton>
<SecondaryButton>Hello World!</SecondaryButton>
```

¿o quizás basta con utilizar una propiedad (prop)?

```jsx
<Button>Hello World!</Button>
<Button variant="secondary">Hello World!</Button>
```

En segundo lugar, ¿Cómo debería de llamarse esta propiedad? ¿variante? ¿tipo? ¿Has tenido en cuenta la posibilidad de que sea un [atributo reservado](https://www.w3schools.com/tags/att_button_type.asp) para la etiqueta HTML `<button>`? Este es el tipo de decisiones que tendrás que tomar cuando construyas tu biblioteca de componentes.

Más preguntas a tener en cuenta:

- ¿Vas a permitir que los consumidores [envíen propiedades CSS](https://evergreen.segment.com/components/layout-primitives) como atributos a los componentes de React, o deben de extender/envolver los componentes para lograr el estilo deseado? Si escoges la primera, ten en cuenta [styled-system](https://github.com/styled-system/styled-system).
- ¿Deberías crear estados de carga [directamente en cada componente](http://react.carbondesignsystem.com/?path=/story/accordion--skeleton), o debería existir un [componente genérico](https://www.npmjs.com/package/react-loading-skeleton) para el estado de carga (spinner, skeleton)?
- ¿Cómo vas a realizar el testeo puramente visual de los componentes? ¿[Snapshot testing](https://jestjs.io/docs/en/snapshot-testing)? ¿[Visual diff testing](https://www.chromaticqa.com/)?
- ¿Cómo van a integrarse tus consumidores con tu librería de componentes? ¿Debería de haber un [reset de estilo global](https://cssreset.com/what-is-a-css-reset/)? ¿Deben de añadir algún [plugin de Babel](https://github.com/ant-design/babel-plugin-import)?
- ¿Deberías de utilizar [[Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/) para auto-generar un documento con los cambios (changelog)?
- ¿Van a necesitar tus componentes soporte para web _y_ móvil? Considera el uso de [React Native Web](https://github.com/devhubapp/devhub) para crear una versión multiplataforma de tu código.

### Escoger una plataforma para la documentación.

Necesitas una plataforma para mostrar el sistema de diseño.
Si acabas de empezar, te recomiendo que uses **[Storybook Docs](https://medium.com/storybookjs/storybook-docs-sneak-peak-5be78445094a)**.

![Storybook Docs](/static/images/guias-de-estilo-diseño-de-sistemas/storybook-docs.png)

You need a platform to display your design system.
If you're just getting started, I would recommend using **[Storybook Docs](https://medium.com/storybookjs/storybook-docs-sneak-peak-5be78445094a)**.

![Storybook Docs](/static/images/guias-de-estilo-diseño-de-sistemas/storybook-docs.png)

Esta combinación te ofrecerá un resultado similar a otras plataformas como [Docz](https://github.com/pedronauck/docz), [Styleguidist](https://react-styleguidist.js.org/), y [Docusaurus](https://docusaurus.io/).

- Copia fácilmente fragmentos de código de otros componentes.
- Genera automáticamente una tabla de atributos y propiedades basandote en prop-types`y`defaultProps`
- Comprueba fácilmente todas las permutaciones de un componente.
- Escribe documentación propia utilizando MDX.

![Storybook Flow](/static/images/guias-de-estilo-diseño-de-sistemas/storybook-flow.png)

Si eres inflexible como se refleja tu marca dentro del sistema de diseño y puedes alimentar tu propia librería, recomiendo crear una solución propia utilizando [MDX](https://mdxjs.com/). Ya que te permitirá insertar componentes de React dentro del propio archivo. El blog que ahora mismo estás leyendo utiliza [eset sistema](https://github.com/leerob/leerob.io).

Tener una solución personalizada te permite tener un control total y absoluto sobre tus documentos. Un ejemplo que verdaderamente me impresiona es [Atomize React](https://atomizecode.com/docs).

![Atomize React](/static/images/guias-de-estilo-diseño-de-sistemas/atomize.png)

### Escribir la documentación del diseño de sistema.

Ahora que has escogido una plataforma para documentar el sistema de diseño, es hora de escribir los documentos. Deberías incluir:

- Instrucciones de instalación y configuración de la librería de componentes.
- Esquemas, definiciones de atributos y propiedades de cada componente.
- Ejemplos visuales de los componentes junto con fragmentos de código para copiar.
- Una justificación del uso de dicha tecnología.

Considera la posibilidad de añadir:

- Una sección de "Cómo empezar" que incluya un tutorial
- Plantillas de interfaz para escoger.
- Información sobre cómo configurar un tema propio.
- Un editor online para interactuar con los componentes.

## Conclusión

He invertido mucho tiempo en el último año investigando sobre diseño de sistemas. Desarrollar una librería de componentes desde cero me ha dado una mejor perspectiva de la cantidad de trabajo que hay detrás del diseño y desarrollo en el apartado front-end de una aplicación.
A continuación, encontrarás una lista de los sistemas de diseño que me han inspirado y me sirven como recursos de gran utilidad.

Siéntete libre de [contactar](mailto:me@sergio.dev) si quieres hablar sobre diseño de sistemas o la traducción de este artículo. O contacta directamente con el [autor](mailto:me@leerob.io).

## Recursos

**Diseño de sistemas**

- [Shopify's Polaris](https://polaris.shopify.com/)
- [Segment's Evergreen](https://evergreen.segment.com/)
- [Eva Design](https://eva.design)
- [Vercel's Design System](https://vercel.com/design)
- [Ant Design](https://ant.design/)
- [IBM's Carbon](https://www.carbondesignsystem.com/)
- [Storybook's Design System](https://github.com/storybookjs/design-system)
- [Modulz's Radix](https://radix.modulz.app/docs/getting-started/)
- [GitHub's Primer](https://primer.style/)
- [Palantir's Blueprint](https://blueprintjs.com/)
- [Atlassian's Design System](https://atlassian.design/)
- [Chakra UI](https://chakra-ui.com/)
