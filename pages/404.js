import React from 'react';
import NextLink from 'next/link';
import {
  useColorMode,
  Heading,
  Text,
  Flex,
  Stack,
  Button
} from '@chakra-ui/core';

import Container from '../components/Container';

const Error = () => {
  const { colorMode } = useColorMode();
  const secondaryTextColor = {
    light: 'gray.700',
    dark: 'gray.400'
  };

  return (
    <Container>
      <Stack
        as="main"
        spacing={8}
        justifyContent="center"
        alignItems="flex-start"
        m="0 auto 4rem auto"
        maxWidth="700px"
      >
        <Flex
          flexDirection="column"
          justifyContent="flex-start"
          alignItems="flex-start"
          maxWidth="700px"
        >
          <Heading letterSpacing="tight" mb={2} as="h1" size="2xl">
            <span role="img" aria-label="Detective">
              🕵️
            </span>
            – Es mi negocio saber lo que otras personas no saben.
          </Heading>
          <Text color={secondaryTextColor[colorMode]} my={4}>
            En efecto mi querido Watson, tenemos un claro caso de error 404. Por
            lo que parece has encontrado algo que solía existir o has cometido
            un error escribiendo. Por lo general, es bastante probable, que
            hayas escrito algo mal. ¿Puedes revisar el enlace?
          </Text>
          <NextLink href="/" passHref>
            <Button
              as="a"
              p={[1, 4]}
              w="250px"
              fontWeight="bold"
              m="3rem auto 0"
            >
              Página principal
            </Button>
          </NextLink>
        </Flex>
      </Stack>
    </Container>
  );
};

export default Error;
