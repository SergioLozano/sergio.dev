import React, { useState } from 'react';
import { NextSeo } from 'next-seo';
import {
  useColorMode,
  Heading,
  Text,
  Flex,
  Stack,
  Input,
  InputGroup,
  InputRightElement,
  Icon
} from '@chakra-ui/core';

import Container from '../components/Container';
import BlogPost from '../components/BlogPost';

// eslint-disable-next-line import/no-unresolved, import/extensions
import { frontMatter as blogPosts } from './blog/**/*.mdx';
import { frontMatter as styleGuides } from './blog/guias-de-estilo-diseño-de-sistemas.mdx';
import { frontMatter as monorepo } from './blog/monorepo-lerna-yarn-workspaces.mdx';
import { frontMatter as styledComponents } from './blog/biblioteca-de-componentes-ui-con-styled-components.mdx';

const url = 'https://sergio.dev/blog';
const title = 'Blog – Sergio Lozano';
const description =
  'Thoughts on the software industry, programming, tech, videography, music, and my personal life.';

const Blog = () => {
  const [searchValue, setSearchValue] = useState('');
  const { colorMode } = useColorMode();
  const secondaryTextColor = {
    light: 'gray.700',
    dark: 'gray.400'
  };

  const filteredBlogPosts = blogPosts
    .sort(
      (a, b) =>
        Number(new Date(b.publishedAt)) - Number(new Date(a.publishedAt))
    )
    .filter((frontMatter) =>
      frontMatter.title.toLowerCase().includes(searchValue.toLowerCase())
    );

  return (
    <>
      <NextSeo
        title={title}
        description={description}
        canonical={url}
        openGraph={{
          url,
          title,
          description
        }}
      />
      <Container>
        <Stack
          as="main"
          spacing={8}
          justifyContent="center"
          alignItems="flex-start"
          m="0 auto 4rem auto"
          maxWidth="700px"
        >
          <Flex
            flexDirection="column"
            justifyContent="flex-start"
            alignItems="flex-start"
            maxWidth="700px"
          >
            <Heading letterSpacing="tight" mb={2} as="h1" size="2xl">
              Blog
            </Heading>
            <Text mb={2} color={secondaryTextColor[colorMode]}>
              Esta es una recopilación personal de los mejores artículos que
              encuentro sobre programación. Poco a poco quiero ir ampliando la
              colección, personalmente me ayuda a organizarme y recordar mejor
              los posts que voy leyendo mes a mes.
            </Text>
            <Text color={secondaryTextColor[colorMode]}>
              {`En total, he recopilado ${blogPosts.length} artículos. Si necesitas
              buscar alguno, puedes utilizar el buscador que se encuentra justo
              debajo.`}
            </Text>
            <InputGroup my={4} mr={4} w="100%">
              <Input
                aria-label="Buscar artículos"
                onChange={(e) => setSearchValue(e.target.value)}
                placeholder="Buscar artículos"
              />
              <InputRightElement>
                <Icon name="search" color="gray.300" />
              </InputRightElement>
            </InputGroup>
          </Flex>
          {!searchValue && (
            <Flex
              flexDirection="column"
              justifyContent="flex-start"
              alignItems="flex-start"
              maxWidth="700px"
              mt={8}
            >
              <Heading letterSpacing="tight" mb={4} size="xl" fontWeight={700}>
                Más recientes
              </Heading>
              <BlogPost {...styleGuides} />
              <BlogPost {...styledComponents} />
              <BlogPost {...monorepo} />
            </Flex>
          )}
          <Flex
            flexDirection="column"
            justifyContent="flex-start"
            alignItems="flex-start"
            maxWidth="700px"
            mt={8}
          >
            <Heading letterSpacing="tight" mb={4} size="xl" fontWeight={700}>
              Todos los artículos
            </Heading>
            {!filteredBlogPosts.length && 'Ningún resultado para tu búsqueda.'}
            {filteredBlogPosts.map((frontMatter) => (
              <BlogPost key={frontMatter.title} {...frontMatter} />
            ))}
          </Flex>
        </Stack>
      </Container>
    </>
  );
};

export default Blog;
